"use strict";

const gulp = require("gulp");
const less = require("gulp-less");
const autoprefixer = require("gulp-autoprefixer");
const cssmin = require("gulp-cssmin");
const concat = require("gulp-concat");
const imagemin = require("gulp-imagemin");


gulp.task("styles", function() {
  return gulp.src("css/*.less")
    .pipe(less())
    .pipe(autoprefixer({
      browsers: ["last 2 versions"]
    }))
    .pipe(concat("style.css"))
    .pipe(cssmin())
    .pipe(gulp.dest("bin/css"))
});

gulp.task("fonts", function() {
  return gulp.src("css/fonts/*.*", {base: "css"})
    .pipe(gulp.dest("bin/css"))
});

gulp.task("images", function() {
  return gulp.src("img/*.*")
    .pipe(imagemin([
      imagemin.gifsicle({interlaced: true}),
      imagemin.jpegtran({progressive: true}),
      imagemin.optipng({optimizationLevel: 5})
    ]))
    .pipe(gulp.dest("bin/img"))
});

gulp.task("index", function() {
  return gulp.src("*.html")
    .pipe(gulp.dest("bin/"))
});

gulp.task("form", function() {
  return gulp.src("*.php")
    .pipe(gulp.dest("bin/"))
});


gulp.task("default", ["index", "styles", "form", "fonts", "images"]);
